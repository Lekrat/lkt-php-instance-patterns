<?php


namespace Lkt\InstancePatterns\Traits;

/**
 * Trait AutomaticInstanceTrait
 * @package Lkt\InstancePatterns\Traits
 */
trait AutomaticInstanceTrait
{
    /**
     * @return bool
     */
    public function isAutomaticInstance() :bool
    {
        return true;
    }
}