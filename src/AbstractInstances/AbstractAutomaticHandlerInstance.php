<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\AutomaticInstanceInterface;
use Lkt\InstancePatterns\Interfaces\HandlerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;

/**
 * Class AbstractAutomaticHandlerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractAutomaticHandlerInstance implements HandlerInterface, AutomaticInstanceInterface
{
    use AutomaticInstanceTrait;
}