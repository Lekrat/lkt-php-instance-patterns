<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\SingleTonTrait;

/**
 * Class AbstractResponseInfo
 * @package Lkt\InstancePatterns\AbstractInstances
 */
class AbstractResponseInfo
{
    use SingleTonTrait;

    protected $codes = [];
    protected $messages = [];

    /**
     * @param int $code
     * @param string $message
     * @return $this
     */
    public function addCode(int $code, string $message)
    {
        if (!in_array($code, $this->codes, true)){
            $this->codes[] = $code;
            $this->messages[$code] = trim($message);
        }

        return $this;
    }

    /**
     * @param int $code
     * @return string
     */
    public function getMessage(int $code)
    {
        if (isset($this->messages[$code])) {
            return $this->messages[$code];
        }
        return '';
    }
}