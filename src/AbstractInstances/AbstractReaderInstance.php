<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\SingleTonTrait;

class AbstractReaderInstance
{
    use SingleTonTrait;

    protected $map = [];

    public function __construct()
    {
        $this->map[static::class] = [];
    }

    public function configureReader(string $key, string $instanceClass, string $crudClass): self
    {
        $this->map[static::class][$key] = [$instanceClass, $crudClass];
        return $this;
    }

    public function readItem(string $key, int $id): array
    {
        if ($this->map[static::class][$key]) {
            $class = '\\'.$this->map[static::class][$key][0];
            $reader = '\\'.$this->map[static::class][$key][1];
            return $reader::getInstance()
                ->setInstance($class::getInstance($id))
                ->read();
        }
        return [];
    }

    public function getItemCrud(string $key, int $id): ?AbstractCrud
    {
        if ($this->map[static::class][$key]) {
            $class = '\\'.$this->map[static::class][$key][0];
            $reader = '\\'.$this->map[static::class][$key][1];
            return $reader::getInstance()
                ->setInstance($class::getInstance($id));
        }
        return null;
    }

    public function getItem(string $key, int $id): ?AbstractCrudInstance
    {
        if ($this->map[static::class][$key]) {
            $class = '\\'.$this->map[static::class][$key][0];
            return $class::getInstance($id);
        }
        return null;
    }

}