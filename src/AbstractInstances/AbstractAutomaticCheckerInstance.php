<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\AutomaticInstanceInterface;
use Lkt\InstancePatterns\Interfaces\CheckerInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;

/**
 * Class AbstractAutomaticHandlerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractAutomaticCheckerInstance implements CheckerInterface, AutomaticInstanceInterface
{
    use AutomaticInstanceTrait;
}