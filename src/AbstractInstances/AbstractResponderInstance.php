<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\ResponderInterface;

/**
 * Class AbstractResponderInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractResponderInstance implements ResponderInterface
{
}