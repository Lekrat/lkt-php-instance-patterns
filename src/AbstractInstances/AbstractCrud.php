<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\InstantiableTrait;
use const Lkt\InstancePatterns\CRUD_STATUS_FAIL;
use const Lkt\InstancePatterns\CRUD_STATUS_OK;

abstract class AbstractCrud
{
    use InstantiableTrait;

    protected $instance;
    protected $lastActionStatus = 0;

    public function __construct(array $params = [])
    {
        $this->hydrate($params);
    }

    /**
     * @return static
     */
    public static function fromData($params): self
    {
        return new static($params);
    }

    /**
     * @return static
     */
    public static function fromInstance(AbstractCrudInstance $instance): self
    {
        return (new static([]))->setInstance($instance);
    }

    /**
     * @param AbstractCrudInstance $instance
     * @return $this
     */
    public function setInstance(AbstractCrudInstance $instance): self
    {
        $this->instance = $instance;
        return $this;
    }

    /**
     * @return $this
     */
    public function setActionStatusOk(): self
    {
        $this->lastActionStatus = CRUD_STATUS_OK;
        return $this;
    }

    /**
     * @return $this
     */
    public function setActionStatusFail(): self
    {
        $this->lastActionStatus = CRUD_STATUS_FAIL;
        return $this;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setActionStatus(int $status): self
    {
        $this->lastActionStatus = $status;
        return $this;
    }

    /**
     * @return bool
     */
    public function statusIsOk(): bool
    {
        return $this->lastActionStatus === CRUD_STATUS_OK;
    }

    /**
     * @return bool
     */
    public function statusIsFail(): bool
    {
        return $this->lastActionStatus === CRUD_STATUS_FAIL;
    }

    /**
     * @param int $status
     * @return bool
     */
    public function statusIs(int $status): bool
    {
        return $this->lastActionStatus === $status;
    }

    /**
     * @param array $params
     * @return $this
     */
    abstract public function hydrate(array $params): self;

    /**
     * @return $this
     */
    abstract public function create(): self;

    /**
     * @return array
     */
    abstract public function read(): array;

    /**
     * @return $this
     */
    abstract public function update(): self;

    /**
     * @return $this
     */
    abstract public function delete(): self;
}