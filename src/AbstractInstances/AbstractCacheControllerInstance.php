<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;

/**
 * Class AbstractCacheControllerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractCacheControllerInstance implements CacheControllerInterface
{

}