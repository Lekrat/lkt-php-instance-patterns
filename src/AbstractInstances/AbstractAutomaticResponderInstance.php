<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\AutomaticInstanceInterface;
use Lkt\InstancePatterns\Interfaces\ResponderInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;

/**
 * Class AbstractAutomaticResponderInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractAutomaticResponderInstance implements ResponderInterface, AutomaticInstanceInterface
{
    use AutomaticInstanceTrait;
}