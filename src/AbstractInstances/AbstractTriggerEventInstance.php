<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractHookInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
class AbstractTriggerEventInstance
{
    use InstantiableTrait,
        AutomaticInstanceTrait;

    protected $args;

    /**
     * AbstractTriggerEventInstance constructor.
     * @param $args
     */
    public function __construct($args)
    {
        $this->args = $args;
    }
}