<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractDeleteCrud
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractDeleteCrud extends AbstractAutomaticHandlerInstance
{
    use InstantiableTrait;
}