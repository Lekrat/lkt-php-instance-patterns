<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\InstantiableTrait;
use function Lkt\Tools\Pagination\getTotalPages;
use function Lkt\Tools\Parse\clearInput;

abstract class AbstractDatabaseLoader
{
    use InstantiableTrait;

    protected $page = -1;
    protected $itemsPerPage = -1;
    protected $orderBy = '';
    protected $filters = [];

    /**
     * @return array
     */
    abstract public function load(): array;

    /**
     * @return int
     */
    abstract public function count(): int;

    /**
     * @return int
     */
    final public function pages(): int
    {
        return getTotalPages($this->count(), $this->itemsPerPage);
    }

    /**
     * @param int $page
     * @param int $itemsPerPage
     * @return $this
     */
    public function pagination(int $page, int $itemsPerPage): self
    {
        $this->page = $page;
        $this->itemsPerPage = $itemsPerPage;
        return $this;
    }

    /**
     * @param string $orderBy
     * @return $this
     */
    public function orderBy(string $orderBy): self
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @param array $filters
     * @return $this
     */
    public function filter(array $filters): self
    {
        $this->filters = $filters;
        return $this;
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildStringLikeFilter(array &$where, $key, $datum)
    {
        $value = clearInput($datum);
        if ($value !== '') {
            $where[] = "{$key} LIKE '%{$datum}%'";
        }
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildStringEqualFilter(array &$where, $key, $datum)
    {
        $value = clearInput($datum);
        if ($value !== '') {
            $where[] = "{$key} = '{$datum}'";
        }
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildIntegerEqualFilter(array &$where, $key, $datum)
    {
        $value = (int)clearInput($datum);
        $where[] = "{$key} = {$datum}";
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildForeignKeyFilter(array &$where, $key, $datum)
    {
        $value = (int)clearInput($datum);
        if ($value > 0) {
            $where[] = "{$key} = {$datum}";
        }
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildForeignKeysFilter(array &$where, $key, $datum)
    {
        $value = clearInput($datum);
        if ($value !== '') {
            $t = [];
            $t[] = "{$key} LIKE '%,{$value},%'";
            $t[] = "{$key} LIKE '%,{$value}'";
            $t[] = "{$key} LIKE '{$value},%'";
            $t[] = "{$key} = '{$value}'";
            $where[] = '(' . implode(' OR ', $t) . ')';
        }
    }

    /**
     * @param array $where
     * @param $key
     * @param $datum
     * @return void
     */
    protected function buildColumnReferenceFilter(array &$where, $key, $datum)
    {
        $value = clearInput($datum);
        if ($value !== '') {
            $where[] = "{$key} = {$datum}";
        }
    }
}