<?php

namespace Lkt\InstancePatterns\BaseInstances;

use Lkt\InstancePatterns\AbstractInstances\AbstractCacheControllerInstance;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;

/**
 * Class BaseCacheControllerInstance
 * @package Lkt\InstancePatterns\BaseInstances
 */
class BaseCacheControllerInstance extends AbstractCacheControllerInstance
{
    use CacheControllerTrait;
}