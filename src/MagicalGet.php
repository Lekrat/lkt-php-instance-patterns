<?php

namespace Lkt\InstancePatterns;

/**
 * Trait MagicalGet
 * @package Lkt\InstancePatterns
 * @deprecated
 */
trait MagicalGet
{
    /**
     * @param $name
     * @return string
     */
    public function __get($name)
    {
        if (isset($this->{$name})){
            return $this->{$name};
        }
        return '';
    }
}