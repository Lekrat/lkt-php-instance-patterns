<?php

namespace Lkt\InstancePatterns;

/**
 * Trait SingleTonWithArgs
 * @package Lkt\InstancePatterns
 * @deprecated
 */
trait SingleTonWithArgs
{
    /**
     * @var static
     */
    protected static $Instance;

    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        if (!\is_object(self::$Instance)){
            self::$Instance = new static(...$args);
            if (\method_exists(self::$Instance, 'PostAwake')){
                self::$Instance->PostAwake();
            }
        }

        return self::$Instance;
    }
}