<?php

namespace Lkt\InstancePatterns;

use Lkt\InstancePatterns\AbstractInstances\AbstractCrud;
use Lkt\InstancePatterns\AbstractInstances\AbstractCrudInstance;
use Lkt\InstancePatterns\AbstractInstances\AbstractReaderInstance;

const CRUD_STATUS_OK = 0;
const CRUD_STATUS_FAIL = 1;

/**
 * @param string $key
 * @param string $instanceClass
 * @param string $crudClass
 * @return AbstractReaderInstance
 */
function configureReader(string $key, string $instanceClass, string $crudClass): AbstractReaderInstance
{
    $reader = AbstractReaderInstance::getInstance();
    return $reader->configureReader($key, $instanceClass, $crudClass);
}

/**
 * @param string $key
 * @param int $id
 * @return AbstractCrud|null
 */
function getReaderCrud(string $key, int $id): ?AbstractCrud
{
    $reader = AbstractReaderInstance::getInstance();
    return $reader->getItemCrud($key, $id);
}

/**
 * @param string $key
 * @param int $id
 * @return AbstractCrudInstance|null
 */
function getReaderItem(string $key, int $id): ?AbstractCrudInstance
{
    $reader = AbstractReaderInstance::getInstance();
    return $reader->getItem($key, $id);
}