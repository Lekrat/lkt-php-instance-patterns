<?php


namespace Lkt\InstancePatterns;

/**
 * Trait HandlerInstance
 * @package Lkt\InstancePatterns
 * @deprecated
 */
trait HandlerInstance
{
    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        $r = new static(...$args);

        if (\method_exists($r, 'PostAwake')){
            return $r->PostAwake();
        }

        return null;
    }
}