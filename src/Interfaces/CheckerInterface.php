<?php

namespace Lkt\InstancePatterns\Interfaces;

/**
 * Interface CheckerInterface
 * @package Lkt\InstancePatterns\Interfaces
 */
interface CheckerInterface
{
    /**
     * @return bool
     */
    public function handle(): bool;
}