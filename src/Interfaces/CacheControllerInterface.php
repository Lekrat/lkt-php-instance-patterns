<?php


namespace Lkt\InstancePatterns\Interfaces;

/**
 * Interface CacheControllerInterface
 * @package Lkt\InstancePatterns\Interfaces
 */
interface CacheControllerInterface
{
    /**
     * @param string $code
     * @param $data
     * @return int
     */
    public static function store(string $code, $data): int;

    /**
     * @param string $code
     * @return mixed
     */
    public static function load(string $code);

    /**
     * @param string $code
     * @return bool
     */
    public static function inCache(string $code): bool;

    /**
     * @return int
     */
    public static function clear(): int;

    /**
     * @param string $code
     * @return int
     */
    public static function clearCode(string $code): int;

    /**
     * @return int
     */
    public static function enable(): int;

    /**
     * @return int
     */
    public static function disable(): int;

    /**
     * @return bool
     */
    public static function isEnabled(): bool;
}