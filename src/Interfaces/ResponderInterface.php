<?php


namespace Lkt\InstancePatterns\Interfaces;


use Lkt\InstancePatterns\Responses\ResponseInstance;

interface ResponderInterface
{
    public function handle() :ResponseInstance;
}